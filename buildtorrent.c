/*
buildtorrent -- torrent file creation program
Copyright (C) 2007-2024 Claude Heiland-Allen


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* GNU source, for versionsort for scandir */
#define _GNU_SOURCE

#include "config.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <dirent.h>
#include <time.h>
/* for correct behaviour ensure this is the POSIX and not the GNU basename:
   "With glibc, one gets the POSIX version of basename() when <libgen.h> is
    included, and the GNU version otherwise." */
#include <libgen.h>

#include "sha1.h"
#include "md5.h"


/******************************************************************************
program version string
******************************************************************************/
#define bt_version "0.9.1"
const char* __version__ = bt_version;

/******************************************************************************
torrent data structure declarations
******************************************************************************/

/* structure for torrent file data */
struct _bt_data;
typedef struct _bt_data *bt_data;

/* error flags */
enum _bt_error {
  BT_ERROR_NONE = 0,
  BT_ERROR_IO,
  BT_ERROR_OVERFLOW,
  BT_ERROR_PARSE
};
typedef enum _bt_error bt_error;

/* attempt to support large files */
typedef int64_t integer;

/* constructors */
bt_data bt_string(const unsigned int length, const char *data);
bt_data bt_integer(const integer number);
bt_data bt_list(void);
bt_data bt_dictionary(void);

/* append to a list */
void bt_list_append(bt_data list, const bt_data data);

/* insert into a dictionary, maintaining sorted keys */
void bt_dictionary_insert(
  bt_data dictionary,
  const unsigned int keylength, const char *keyname,
  const bt_data data
);

/* deep-copy a structure */
bt_data bt_copy(const bt_data data);

/* write as a torrent file */
bt_error bt_write(FILE *f, const bt_data bd, int toplevel);

/* types of structures */
enum _bt_type {
  BT_TYPE_STRING = 1000,
  BT_TYPE_INTEGER,
  BT_TYPE_LIST,
  BT_TYPE_DICTIONARY
};
typedef enum _bt_type bt_type;

/* string structure */
struct _bt_string {
  unsigned int length;
  char *data;
};

/* integer structure */
struct _bt_integer {
  integer number;
};

/* list structure */
struct _bt_list {
  struct _bt_data *item;
  struct _bt_list *next;
};

/* dictionary structure */
struct _bt_dictionary {
  struct _bt_string *key;
  struct _bt_data *value;
  struct _bt_dictionary *next;
};

/* general structure */
struct _bt_data {
  bt_type type;
  union {
    struct _bt_string *string;
    struct _bt_integer *integer;
    struct _bt_list *list;
    struct _bt_dictionary *dictionary;
  } b;
};

/******************************************************************************
abort on memory allocation failure
******************************************************************************/
void *bt_malloc(size_t size) {
  void *mem = malloc(size);
  if (!mem) {
    fprintf(stderr, "buildtorrent: out of memory\n");
    exit(1);
  }
  return mem;
}

/******************************************************************************
allocate a new string structure
******************************************************************************/
bt_data bt_string(const unsigned int length, const char *data) {
  bt_data bd = bt_malloc(sizeof(struct _bt_data));
  struct _bt_string *s = bt_malloc(sizeof(struct _bt_string));
  s->data = bt_malloc(length);
  s->length = length;
  memcpy(s->data, data, length);
  bd->type = BT_TYPE_STRING;
  bd->b.string = s;
  return bd;
}
#define bt_string0(s) bt_string(strlen((s)),(s))

/******************************************************************************
allocate a new integer structure
******************************************************************************/
bt_data bt_integer(const integer number) {
  bt_data bd = bt_malloc(sizeof(struct _bt_data));
  struct _bt_integer *n = bt_malloc(sizeof(struct _bt_integer));
  n->number = number;
  bd->type = BT_TYPE_INTEGER;
  bd->b.integer = n;
  return bd;
}

/******************************************************************************
allocate a new empty list structure
invariant: bd->b.list != NULL && last(bd->b.list).{item, next} == NULL
******************************************************************************/
bt_data bt_list(void) {
  bt_data bd = bt_malloc(sizeof(struct _bt_data));
  struct _bt_list *l = bt_malloc(sizeof(struct _bt_list));
  l->item = NULL;
  l->next = NULL;
  bd->type = BT_TYPE_LIST;
  bd->b.list = l;
  return bd;
}

/******************************************************************************
allocate a new empty dictionary structure
invariant: bd->b.dictionary != NULL &&
           last(bd->b.list).{key, value, next} == NULL &&
	   ordered ascending by key
******************************************************************************/
bt_data bt_dictionary(void) {
  bt_data bd = bt_malloc(sizeof(struct _bt_data));
  struct _bt_dictionary *d = bt_malloc(sizeof(struct _bt_dictionary));
  d->key = NULL;
  d->value = NULL;
  d->next = NULL;
  bd->type = BT_TYPE_DICTIONARY;
  bd->b.dictionary = d;
  return bd;
}

/******************************************************************************
precondition: list is a valid list
append an item to a list
the item is not copied
******************************************************************************/
void bt_list_append(bt_data list, const bt_data data) {
  assert(list);
  assert(BT_TYPE_LIST == list->type);
  assert(list->b.list);
  {
  struct _bt_list *current;
  struct _bt_list *end = bt_malloc(sizeof(struct _bt_list));
  end->item = NULL;
  end->next = NULL;
  current = list->b.list;
  while (current->next) {
    current = current->next;
  }
  current->item = data;
  current->next = end;
  }
}

/******************************************************************************
precondition: list is a valid list
compute length of list
******************************************************************************/
integer bt_list_length(const bt_data list) {
  assert(list);
  assert(BT_TYPE_LIST == list->type);
  assert(list->b.list);
  {
  const struct _bt_list *current = list->b.list;
  integer length = 0;
  while (current->next) {
    length++;
    current = current->next;
  }
  return length;
  }
}

/******************************************************************************
precondition: dictionary is a valid dictionary
insert an item into a dictionary
the value is not copied, the key is copied
maintains an ascending ordering of key names
FIXME: assumes key names are null terminated
******************************************************************************/
void bt_dictionary_insert(
  bt_data dictionary,
  const unsigned int keylength, const char *keyname,
  const bt_data data
) {
  assert(dictionary);
  assert(BT_TYPE_DICTIONARY == dictionary->type);
  assert(dictionary->b.dictionary);
  {
  struct _bt_dictionary *current;
  struct _bt_dictionary *lastcurrent = NULL;
  struct _bt_dictionary *insertee;
  struct _bt_string *key = bt_malloc(sizeof(struct _bt_string));
  key->data = bt_malloc(keylength);
  insertee = bt_malloc(sizeof(struct _bt_dictionary));
  memcpy(key->data, keyname, keylength);
  key->length = keylength;
  insertee->key = key;
  insertee->value = data;
  insertee->next = NULL;
  current = dictionary->b.dictionary;
  while (
    current->next && current->key && (0 < strcmp(keyname, current->key->data))
  ) {
    lastcurrent = current;
    current = current->next;
  }
  if (lastcurrent) {
    insertee->next = current;
    lastcurrent->next = insertee;
  } else {
    insertee->next = dictionary->b.dictionary;
    dictionary->b.dictionary = insertee;
  }
  }
}

#define bt_dictionary_insert0(dict,key,data) \
bt_dictionary_insert((dict),strlen((key)),(key),(data))

/******************************************************************************
precondition: data is valid
deep copy a data structure
FIXME: doesn't handle dictionaries yet
******************************************************************************/
bt_data bt_copy(const bt_data data) {
  assert(data);
  switch (data->type) {
  case (BT_TYPE_STRING): {
    return bt_string(data->b.string->length, data->b.string->data);
  }
  case (BT_TYPE_INTEGER): {
    return bt_integer(data->b.integer->number);
  }
  case (BT_TYPE_LIST): {
    struct _bt_list *current;
    bt_data list = bt_list();
    current = data->b.list;
    while (current && current->item) {
      bt_list_append(list, bt_copy(current->item));
      current = current->next;
    }
    return list;
  }
  default: {
    fprintf(stderr, "buildtorrent: BUG! can't copy this\n");
    exit(1);
  }
  }
}

/******************************************************************************
precondition: s is valid
write a string in torrent encoding
******************************************************************************/
bt_error bt_write_string(FILE *f, const struct _bt_string *s) {
  assert(s);
  assert(s->data);
  if (0 > fprintf(f, "%d:", s->length)) {
    return BT_ERROR_IO;
  }
  if (1 != fwrite(s->data, s->length, 1, f)) {
    return BT_ERROR_IO;
  }
  return BT_ERROR_NONE;
}

/******************************************************************************
precondition: bd is valid
write a data structure in torrent encoding
******************************************************************************/
long _bt_write_info_start;
long _bt_write_info_end;

bt_error bt_write(FILE *f, const bt_data bd, int toplevel) {
  assert(bd);
  if (toplevel) {
    _bt_write_info_start = -1;
    _bt_write_info_end = -1;
  }
  switch (bd->type) {
  case (BT_TYPE_STRING): {
    return bt_write_string(f, bd->b.string);
    break;
  }
  case (BT_TYPE_INTEGER): {
    assert(bd->b.integer);
    if (0 > fprintf(f, "i%" PRId64 "e", bd->b.integer->number)) {
      return BT_ERROR_IO;
    }
    break;
  }
  case (BT_TYPE_LIST): {
    struct _bt_list *current;
    bt_error err;
    current = bd->b.list;
    if (0 > fprintf(f, "l")) {
      return BT_ERROR_IO;
    }
    while (current->next) {
      if (current->item) {
        if ((err = bt_write(f, current->item, 0))) {
          return err;
        }
      }
      current = current->next;
    }
    if (0 > fprintf(f, "e")) {
      return BT_ERROR_IO;
    }
    break;
  }
  case (BT_TYPE_DICTIONARY): {
    struct _bt_dictionary *current;
    bt_error err;
    current = bd->b.dictionary;
    if (0 > fprintf(f, "d")) {
      return BT_ERROR_IO;
    }
    while (current->next) {
      if (current->key) {
        if ((err = bt_write_string(f, current->key))) {
          return err;
        }
        if (toplevel && 4 == current->key->length && strncmp("info", current->key->data, current->key->length) == 0) {
          _bt_write_info_start = ftell(f);
        }
        if ((err = bt_write(f, current->value, 0))) {
          return err;
        }
        if (toplevel && 4 == current->key->length && strncmp("info", current->key->data, current->key->length) == 0) {
          _bt_write_info_end = ftell(f);
        }
      }
      current = current->next;
    }
    if (0 > fprintf(f, "e")) {
      return BT_ERROR_IO;
    }
    break;
  }
  default: {
    fprintf(stderr, "buildtorrent: BUG! can't write this\n");
    exit(1);
    break;
  }
  }
  return BT_ERROR_NONE;
}

/******************************************************************************
pretty print torrent data
******************************************************************************/
void bt_show(
  bt_data bd, int piecestoo, int indent, int indentstep, int comma
) {
  int i;
  if (!bd) {
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("NULL%s\n", comma ? "," : "");
    return;
  }
  switch (bd->type) {
  case (BT_TYPE_STRING): {
    char strbuf[512];
    int len = bd->b.string->length > 500 ? 500 : bd->b.string->length;
    memcpy(strbuf, bd->b.string->data, len);
    strbuf[len] = '\0';
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf(
      "\"%s\"%s%s\n", strbuf, comma ? "," : "",
      len == 500 ? " // truncated!" : ""
    );
    break;
  }
  case (BT_TYPE_INTEGER): {
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("%" PRId64 "%s\n", bd->b.integer->number, comma ? "," : "");
    break;
  }
  case (BT_TYPE_LIST): {
    struct _bt_list *current;
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("[\n");
    current = bd->b.list;
    while (current->next) {
      bt_show(
        current->item, piecestoo,
        indent + indentstep, indentstep,
        current->next->next != NULL
      );
      current = current->next;
    }
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("]%s\n", comma ? "," : "");
    break;
  }
  case (BT_TYPE_DICTIONARY): {
    struct _bt_dictionary *current;
    char strbuf[512];
    int len;
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("{\n");
    current = bd->b.dictionary;
    while (current->next) {
      len = current->key->length > 500 ? 500 : current->key->length;
      memcpy(strbuf, current->key->data, len);
      strbuf[len] = '\0';
      for(i = 0; i < indent + indentstep; i++) {
        printf(" ");
      }
      printf("\"%s\" =>%s\n", strbuf, len == 500 ? " // truncated!" : "");
      if (strcmp("pieces", strbuf) == 0) {
        if (piecestoo) {
          char *hexdigits = "0123456789abcdef";
          printf("----------------------------------------");
          for (i = 0; i < (int) current->value->b.string->length; i++) {
            if ((i % 20) == 0) {
              printf("\n");
            }
            printf("%c%c",
              hexdigits[(current->value->b.string->data[i] & 0xF0) >> 4],
              hexdigits[(current->value->b.string->data[i] & 0x0F)]
            );
          }
          printf("\n----------------------------------------\n");
        } else {
          for(i = 0; i < indent + indentstep + indentstep; i++) {
            printf(" ");
          }
          printf(
            "\"...\"%s // pieces not shown\n", current->next->next ? "," : ""
          );
        }
      } else {
        bt_show(
          current->value, piecestoo, indent + indentstep + indentstep,
          indentstep, current->next->next != NULL
        );
      }
      current = current->next;
    }
    for(i = 0; i < indent; i++) {
      printf(" ");
    }
    printf("}%s\n", comma ? "," : "");
    break;
  }
  }
}


/******************************************************************************
file list data structure
the first in the list is a dummy
******************************************************************************/
struct _bt_file_list {
  char *file;
  bt_data path;
  struct _bt_file_list *next;
};
typedef struct _bt_file_list *bt_file_list;

/******************************************************************************
create the dummy first list element
******************************************************************************/
bt_file_list bt_create_file_list() {
  bt_file_list flist = bt_malloc(sizeof(struct _bt_file_list));
  flist->file = NULL;
  flist->path = NULL;
  flist->next = NULL;
  return flist;
}

/******************************************************************************
precondition: flist and file must be valid
prepend to the file list
copies the arguments
path may be NULL, but only in single file mode
******************************************************************************/
void bt_file_list_prepend(
  bt_file_list flist, const char *file, bt_data path
) {
  assert(flist);
  assert(file);
  {
  bt_file_list node = bt_malloc(sizeof(struct _bt_file_list));
  node->file = bt_malloc(strlen(file) + 1);
  memcpy(node->file, file, strlen(file) + 1);
  if (path) {
    node->path = bt_copy(path);
  } else {
    node->path = NULL;
  }
  node->next = flist->next;
  flist->next = node;
  }
}


/******************************************************************************
annotated file list data structure
the first in the list is a dummy
******************************************************************************/
struct _bt_afile_list {
  char *file;
  bt_data path;
  integer length;
  bt_data md5sum;
  struct _bt_afile_list *next;
};
typedef struct _bt_afile_list *bt_afile_list;

/******************************************************************************
create the dummy first list element
******************************************************************************/
bt_afile_list bt_create_afile_list() {
  bt_afile_list aflist = bt_malloc(sizeof(struct _bt_afile_list));
  aflist->file = NULL;
  aflist->path = NULL;
  aflist->length = 0;
  aflist->md5sum = NULL;
  aflist->next = NULL;
  return aflist;
}

/******************************************************************************
preconditions: aflist and fnode are valid
prepend to the annotated file list
aflist is an annotated file list to prepend to
fnode is a file list node to annotate
length is the length to annotate with
md5sum is the md5sum to annotate with (may be NULL)
NOTE does NOT create a new copy of the existing data
******************************************************************************/
void bt_afile_list_prepend(
  bt_afile_list aflist, bt_file_list fnode, integer length, bt_data md5sum
) {
  assert(aflist);
  assert(fnode);
  {
  bt_afile_list node = bt_malloc(sizeof(struct _bt_afile_list));
  node->file = fnode->file;
  node->path = fnode->path;
  node->length = length;
  node->md5sum = md5sum;
  node->next = aflist->next;
  aflist->next = node;
  }
}


/******************************************************************************
append a file to a path string
path must be at least length maxlength bytes
returns an error flag
******************************************************************************/
bt_error bt_joinpath(size_t maxlength, char *path, const char *file) {
  if (strlen(path) + strlen(file) + 1 + 1 > maxlength) {
    return BT_ERROR_OVERFLOW;
  } else {
    size_t oldlength = strlen(path);
    if (oldlength > 0) {
      path[oldlength] = '/';
      memcpy(path + oldlength + 1, file, strlen(file) + 1);
    } else {
      memcpy(path, file, strlen(file) + 1);
    }
    return BT_ERROR_NONE;
  }
}


/******************************************************************************
md5sum a file
******************************************************************************/
bt_data bt_md5sum_file(const char *filename, integer length) {
  struct MD5Context context;
  char *hexdigits = "0123456789abcdef";
  unsigned char *buffer = NULL;
  unsigned char digest[16];
  char hexdump[33];
  integer buflen = 262144;
  integer size = 0;
  integer left = length;
  integer i;
  FILE *file = NULL;
  if (!filename) {
    return NULL;
  }
  buffer = bt_malloc(buflen);
  if (!(file = fopen(filename, "rb"))) {
    free(buffer);
    return NULL;
  }
  MD5Init(&context);
  while (left > 0) {
    if (left > buflen) {
       size = buflen;
    } else {
       size = left;
    }
    if (1 != fread(buffer, size, 1, file)) {
      free(buffer);
      fclose(file);
      return NULL;
    }
    MD5Update(&context, buffer, size);
    left -= size;
  }
  MD5Final(digest, &context);
  for (i = 0; i < 16; ++i) {
    hexdump[i+i+0] = hexdigits[(digest[i] & 0xF0) >> 4];
    hexdump[i+i+1] = hexdigits[(digest[i] & 0x0F)];
  }
  free(buffer);
  fclose(file);
  return bt_string(32, hexdump);
}


/******************************************************************************
find files recursively
directory is the initial directory
path is a buffer of maxlength bytes to store the accumulated path
pathlist is the accumulated path exploded as a list
files is a file list to add the found files to
returns an error flag
******************************************************************************/
bt_error bt_find_files(
  int (*compar)(const struct dirent **, const struct dirent **),
  size_t maxlength, char *path, bt_data pathlist, bt_file_list files
) {
  struct dirent **entry;
  bt_data filename = NULL;
  bt_data filepath = NULL;
  bt_error err = BT_ERROR_NONE;
  int i, n;
  /* use scandir instead of readdir for sorted listings */
  i = scandir(path, &entry, 0, compar);
  if (i >= 0) {
  for (n = 0; n < i; n++) {
    if (strcmp(entry[n]->d_name, ".") && strcmp(entry[n]->d_name, "..")) {
      struct stat s;
      size_t oldlength = strlen(path);
      if (!(err = bt_joinpath(maxlength, path, entry[n]->d_name))) {
        if (!stat(path, &s)) {
          if (S_ISREG(s.st_mode)) {
            filename = bt_string0(entry[n]->d_name);
            filepath = bt_copy(pathlist);
            bt_list_append(filepath, filename);
            bt_file_list_prepend(files, path, filepath);
          } else if (S_ISDIR(s.st_mode)) {
            filename = bt_string0(entry[n]->d_name);
            filepath = bt_copy(pathlist);
            bt_list_append(filepath, filename);
            err = bt_find_files(compar, maxlength, path, filepath, files);
          } else {
            /* FIXME neither regular file nor directory, what to do? */
          }
        } else {
          err = BT_ERROR_IO;
        }
      }
      path[oldlength] = '\0';
    }
    if (err) {
      break;
    }
  }
  } else {
    err = BT_ERROR_IO;
  }
  return err;
}


/******************************************************************************
annotate a file list
files is the file list to annotate
afiles is the file list to add to
returns an error flag
******************************************************************************/
bt_error bt_annotate_files(
  bt_file_list files, bt_afile_list afiles
) {
  bt_error err = BT_ERROR_NONE;
  bt_file_list fnode = files;
  while ((fnode = fnode->next)) {
    struct stat s;
    if (!stat(fnode->file, &s)) {
      if (S_ISREG(s.st_mode)) {
        integer length = s.st_size;
        bt_afile_list_prepend(afiles, fnode, length, NULL);
      } else {
        err = BT_ERROR_IO;
      }
    } else {
      err = BT_ERROR_IO;
    }
    if (err) {
      break;
    }
  }
  return err;
}


/******************************************************************************
convert an annotated file list into torrent format
aflist is the list to convert
returns the torrent format file list
******************************************************************************/
bt_data bt_afile_list_info(bt_afile_list aflist) {
  bt_afile_list node;
  bt_data files;
  files = bt_list();
  node = aflist;
  while ((node = node->next)) {
    bt_data file;
    bt_data filesize;
    bt_data filepath;
    bt_data md5sum;
    file = bt_dictionary();
    filesize = bt_integer(node->length);
    if (!(filepath = node->path)) {
      fprintf(stderr, "buildtorrent: BUG! path missing from node\n");
      exit(1);
    }
    bt_dictionary_insert0(file, "length", filesize);
    bt_dictionary_insert0(file, "path", filepath);
    if ((md5sum = node->md5sum)) {
      bt_dictionary_insert0(file, "md5sum", md5sum);
    }
    bt_list_append(files, file);
  }
  return files;
}


/******************************************************************************
draw progressbar
******************************************************************************/
void bt_progressbar(integer piece, integer count) {
  integer oldblocks = ((piece - 1) * 50) / count;
  integer blocks = (piece * 50) / count;
  integer i;
  char s[53];
  if (blocks != oldblocks) {
    s[0] = '[';
    for (i = 0; i < 50; i++) {
      s[i+1] = (i < blocks) ? '=' : ((i == blocks) ? '>' : ' ');
    }
    s[51] = ']';
    s[52] = '\0';
    fprintf(
      stderr,
      "\b\b\b\b\b\b\b\b\b\b"
      "\b\b\b\b\b\b\b\b\b\b"
      "\b\b\b\b\b\b\b\b\b\b"
      "\b\b\b\b\b\b\b\b\b\b"
      "\b\b\b\b\b\b\b\b\b\b"
      "\b\b%s",
      s
    );
  }
}


/******************************************************************************
hash files
return piece hash as a string
data side effect: if domd5sum then add md5sums to aflist
output side effect: if verbose then show file summary
******************************************************************************/
bt_data bt_hash_pieces(
  bt_afile_list aflist, integer size, int domd5sum, int verbose
) {
  bt_afile_list node = NULL;
  char *hashdata = NULL;
  integer total = 0;
  unsigned char *buffer = NULL;
  unsigned char *bufptr = NULL;
  integer remain = size;
  integer left;
  FILE *file = NULL;
  integer piececount;
  integer i;
  sha1_byte digest[SHA1_DIGEST_LENGTH];
  if (!aflist) {
    return NULL;
  }
  buffer = bt_malloc(size);
  node = aflist;
  while ((node = node->next)) {
    if (verbose) {
      fprintf(stderr, "%20" PRId64 " : %s\n", node->length, node->file);
    }
    if (domd5sum) {
      if (!(node->md5sum = bt_md5sum_file(node->file, node->length))) {
        fprintf(
          stderr,
          "buildtorrent: error computing md5sum for \"%s\"\n",
          node->file
        );
      }
    }
    total += node->length;
  }
  piececount = (total + size - 1) / size; /* ceil(total/size) */
  if (piececount <= 0) { /* FIXME: no idea what to do if there's no data */
    free(buffer);
    fprintf(stderr, "torrent has no data, aborting!\n");
    return NULL;
  }
  if (verbose) {
    fprintf(stderr, "hashing %" PRId64 " pieces\n", piececount);
    fprintf(stderr, "["
                    "          "
                    "          "
                    "          "
                    "          "
                    "          "
                    "]");
  }
  hashdata = bt_malloc(piececount * SHA1_DIGEST_LENGTH);
  node = aflist->next;
  file = fopen(node->file, "rb");
  if (!file) {
    free(buffer);
    return NULL;
  }
  left = node->length;
  bufptr = buffer;
  for (i = 0; i < piececount; ++i) {
    do {
      if (left <= remain) {
        /* take all */
        if (left != 0) { /* don't fail on empty files */
          if (1 != fread(bufptr, left, 1, file)) {
            fclose(file);
            free(buffer);
            return NULL;
          }
          bufptr += left;
          remain -= left;
          fclose(file);
          file = NULL;
        }
        node = node->next;
        if (node) {
          file = fopen(node->file, "rb");
          if (!file) {
            free(buffer);
            return NULL;
          }
          left = node->length;
        }
      } else { /* left > remain */
        /* take as much as we can */
        if (remain != 0) { /* don't fail on empty files */
          if (1 != fread(bufptr, remain, 1, file)) {
            free(buffer);
            return NULL;
          }
          bufptr += remain;
          left -= remain;
          remain = 0;
        }
      }
    } while (remain != 0 && node);
    if (!node && i != piececount - 1) {
      /* somehow the pieces don't add up */
      if (file) {
        fclose(file);
      }
      free(buffer);
      return NULL;
    }
    /* remain == 0 || i == piececount - 1 */
    if (verbose) {
      bt_progressbar(i, piececount);
    }
    SHA1(buffer, size - remain, digest);
    memcpy(hashdata + i * SHA1_DIGEST_LENGTH, digest, SHA1_DIGEST_LENGTH);
    bufptr = buffer;
    remain = size;
  }
  if (verbose) {
    bt_progressbar(piececount, piececount);
    fprintf(stderr, "\n");
  }
  return bt_string(SHA1_DIGEST_LENGTH * piececount, hashdata);
}


/******************************************************************************
parse an announce list
format = "url|url|url,url|url|url,url|url|url"
******************************************************************************/
bt_data bt_parse_announcelist(const char *urls) {
  bt_data announcelist;
  bt_data tier;
  const char *s;
  const char *t;
  const char *t1;
  const char *t2;
  if (!urls) {
    return NULL;
  }
  if (strcmp("", urls) == 0) {
    return NULL;
  }
  announcelist = bt_list();
  s = urls;
  tier = bt_list();
  do {
    t = NULL;
    t1 = strchr(s, '|');
    t2 = strchr(s, ',');
    if (!t1 && !t2) {
      t = s + strlen(s);
    } else if (!t1) {
      t = t2;
    } else if (!t2) {
      t = t1;
    } else {
      t = (t1 < t2) ? t1 : t2;
    }
    if (t <= s) {
      return NULL;
    }
    bt_list_append(tier, bt_string(t - s, s));
    if (t[0] == ',' || t[0] == '\0') {
      bt_list_append(announcelist, tier);
      if (t[0] != '\0') {
        tier = bt_list();
      } else {
        tier = NULL;
      }
    }
    s = t + 1;
  } while (t[0] != '\0');
  return announcelist;
}

/******************************************************************************
parse a nodes list
format = "host|port,host|port,host|port"
******************************************************************************/
bt_data bt_parse_nodes(const char *str) {
  bt_data nodes;
  bt_data node;
  const char *s;
  const char *t;
  const char *t1;
  const char *t2;
  if (!str) {
    return NULL;
  }
  if (strcmp("", str) == 0) {
    return NULL;
  }
  nodes = bt_list();
  s = str;
  node = bt_list();
  do {
    t = NULL;
    t1 = strchr(s, '|');
    t2 = strchr(s, ',');
    if (!t1 && !t2) {
      t = s + strlen(s);
    } else if (!t1) {
      t = t2;
    } else if (!t2) {
      t = t1;
    } else {
      t = (t1 < t2) ? t1 : t2;
    }
    if (t <= s) {
      return NULL;
    }
    if (bt_list_length(node) == 0)
    {
      bt_list_append(node, bt_string(t - s, s));
    }
    else if (bt_list_length(node) == 1)
    {
      char *endptr;
      integer port = strtol(s, &endptr, 10);
      if (errno != 0)
      {
        fprintf(stderr, "buildtorrent: error parsing port number\n");
        return NULL;
      }
      if (endptr == s)
      {
        fprintf(stderr, "buildtorrent: empty port number\n");
        return NULL;
      }
      if (endptr != t)
      {
        fprintf(stderr, "buildtorrent: junk after port number\n");
        return NULL;
      }
      bt_list_append(node, bt_integer(port));
    }
    else
    {
      fprintf(stderr, "buildtorrent: too much in node specification\n");
      return NULL;
    }
    if (t[0] == ',' || t[0] == '\0') {
      if (bt_list_length(node) == 2)
      {
        bt_list_append(nodes, node);
      }
      else
      {
        fprintf(stderr, "buildtorrent: not enough in node specification\n");
        return NULL;
      }
      if (t[0] != '\0') {
        node = bt_list();
      } else {
        node = NULL;
      }
    }
    s = t + 1;
  } while (t[0] != '\0');
  return nodes;
}

/******************************************************************************
parse a webseed list
format = "url,url,url"
******************************************************************************/
bt_data bt_parse_webseedlist(const char *urls) {
  bt_data webseedlist;
  const char *s;
  const char *t;
  const char *t2;
  if (!urls) {
    return NULL;
  }
  if (strcmp("", urls) == 0) {
    return NULL;
  }
  webseedlist = bt_list();
  s = urls;
  do {
    t = NULL;
    t2 = strchr(s, ',');
    if (!t2) {
      t = s + strlen(s);
    } else {
      t = t2;
    }
    if (t <= s) {
      return NULL;
    }
    bt_list_append(webseedlist, bt_string(t - s, s));
    s = t + 1;
  } while (t[0] != '\0');
  return webseedlist;
}

/******************************************************************************
read and parse a filelist file
line format = "real/file/system/path|tor/rent/path/file"
delimiters: '|', '\n'
escape character: '\\'
******************************************************************************/
enum _bt_parse_state {
  BT_PARSE_LEFT = 0,
  BT_PARSE_LEFT_ESCAPED,
  BT_PARSE_RIGHT,
  BT_PARSE_RIGHT_ESCAPED,
  BT_PARSE_OK,
  BT_PARSE_ERROR
};
typedef enum _bt_parse_state bt_parse_state;
bt_error bt_parse_filelist(bt_file_list flist, FILE *infile) {
  char filename[8192];
  char torrname[8192];
  bt_data torrpath = NULL;
  int c;
  int i = 0;
  int state = BT_PARSE_LEFT;
  while(state != BT_PARSE_OK && state != BT_PARSE_ERROR) {
    c = getc(infile);
    switch (state) {
    case (BT_PARSE_LEFT):
      if (c < 0) {
        state = BT_PARSE_OK;
      } else if (c == '\\') {
        state = BT_PARSE_LEFT_ESCAPED;
      } else if (c == '|') {
        filename[i] = '\0';
        i = 0;
        torrpath = bt_list();
        state = BT_PARSE_RIGHT;
      } else {
        filename[i++] = c;
        if (i > 8190) {
          state = BT_PARSE_ERROR;
        } else {
          state = BT_PARSE_LEFT;
        }
      }
      break;
    case (BT_PARSE_LEFT_ESCAPED):
      if (c < 0) {
        state = BT_PARSE_ERROR;
      } else {
        filename[i++] = c;
        if (i > 8190) {
          state = BT_PARSE_ERROR;
        } else {
          state = BT_PARSE_LEFT;
        }
      }
      break;
    case (BT_PARSE_RIGHT):
      if (c < 0) {
        state = BT_PARSE_ERROR;
      } else if (c == '\\') {
        state = BT_PARSE_RIGHT_ESCAPED;
      } else if (c == '/' || c == '\n') {
        bt_data torrnamestr;
        torrname[i] = '\0';
        i = 0;
        if (0 == strcmp("", torrname)) {
          state = BT_PARSE_ERROR;
        } else {
          torrnamestr = bt_string0(torrname);
          bt_list_append(torrpath, torrnamestr);
          if (c == '\n') {
            bt_file_list_prepend(flist, filename, torrpath);
            state = BT_PARSE_LEFT;
          } else {
            state = BT_PARSE_RIGHT;
          }
        }
      } else {
        torrname[i++] = c;
        if (i > 8190) {
          state = BT_PARSE_ERROR;
        } else {
          state = BT_PARSE_RIGHT;
        }
      }
      break;
    case (BT_PARSE_RIGHT_ESCAPED):
      if (c < 0) {
        state = BT_PARSE_ERROR;
      } else {
        torrname[i++] = c;
        if (i > 8190) {
          state = BT_PARSE_ERROR;
        } else {
          state = BT_PARSE_RIGHT;
        }
      }
      break;
    default:
      fprintf(
        stderr,
        "buildtorrent: BUG! internal error parsing file list (%d)\n",
        state
      );
      exit(1);
    }
  }
  if (state == BT_PARSE_OK) {
    return BT_ERROR_NONE;
  } else {
    return BT_ERROR_PARSE;
  }
}

/******************************************************************************
show usage message
******************************************************************************/
void bt_usage(void) {
  printf(
    "Usage:\n"
    "  buildtorrent [OPTIONS] input output\n"
    "  buildtorrent [OPTIONS] -f filelist -n name output\n"
    "\n"
    "options:\n"
  );
  printf(
    "--announce      -a  <announce>   : announce url\n"
    "--filelist      -f  <filelist>   : external file list (requires '-n')\n"
    "--name          -n  <name>       : torrent name, default based on input\n"
    "--announcelist  -A  <announces>  : announce url list (format: a,b1|b2,c)\n"
    "--nodes         -N  <nodes>      : DHT nodes list (format: host|port,h|p)\n"
    "--webseeds      -w  <webseeds>   : webseed url list (format: a,b,c)\n"
  );
  printf(
    "--piecelength   -l  <length>     : piece length in bytes, default 262144\n"
    "--piecesize     -L  <size>       : use 2^size as piece length, default 18\n"
    "--comment       -c  <comment>    : user comment, omitted by default\n"
    "--private       -p  <private>    : private flag, either 0 or 1\n"
    "--date          -d  <timestamp>  : set 'creation date' (secs since epoch)\n"
    "--nodate        -D               : omit 'creation date' field\n"
  );
  printf(
    "--createdby     -b  <creator>    : set 'created by' field\n"
    "--nocreatedby   -B               : omit 'created by' field\n"
    "--md5sum        -m               : add an 'md5sum' field for each file\n"
    "--sort          -s <sortorder>   : sort files ([U]nsorted, "
#ifdef _GNU_SOURCE
    "[A]lpha, [V]ersion)\n"
#else
    "[A]lpha)\n"
#endif
    "--quiet         -q               : quiet operation\n"
    "--verbose       -v               : verbose. More -v means more verbose\n"
    "--version       -V               : show version of buildtorrent\n"
    "--help          -h               : show this help screen\n"
  );
}

/******************************************************************************
main program
******************************************************************************/
int main(int argc, char **argv) {

  char *url = NULL;
  char *urls = NULL;
  char *nodesstr = NULL;
  char *wurls = NULL;
  char *inname = NULL;
  char *nameflag = NULL;
  char *namebase = NULL;
  char *outfile = NULL;
  char *commentstr = NULL;
  char *filelistfilename = NULL;
#ifdef _GNU_SOURCE
  char sort = 'v'; /* use version sort, if available */
#else
  char sort = 'a'; /* otherwise use alpha sort */
#endif
  /* default to no sorting (as before) */
  int (*compar)(const struct dirent **, const struct dirent **) = 0;
  int lplen = -1;
  unsigned int plen = 262144;
  int verbose = 1;
  time_t date = time(NULL);
  int nodate = 0;
  const char *creator = "buildtorrent/" bt_version;
  int nocreator = 0;
  int privated = 0;
  int privateopt = 0;
  int domd5sum = 0;
  int magnet = 0;
  int show = 0;
  int slen;
  int i;

  FILE *output = NULL;
  bt_data torrent = NULL;
  bt_data announcelist = NULL;
  bt_data nodes = NULL;
  bt_data webseedlist = NULL;
  bt_data info = NULL;
  bt_data pieces = NULL;
  bt_data pathlist = NULL;

  int multifile = 0;
  bt_file_list flist = NULL;
  bt_afile_list aflist = NULL;

  struct stat s;
  char path[8192];
  char nametemp[8192];

  while (1) {
    int optidx = 0;
    static struct option options[] = {
      { "announce", 1, 0, 'a' },
      { "filelist", 1, 0, 'f' },
      { "name", 1, 0, 'n' },
      { "announcelist", 1, 0, 'A' },
      { "nodes", 1, 0, 'N' },
      { "webseeds", 1, 0, 'w' },
      { "piecelength", 1, 0, 'l' },
      { "piecesize", 1, 0, 'L' },
      { "comment", 1, 0, 'c' },
      { "private", 1, 0, 'p' },
      { "sort", 1, 0, 's' },
      { "date", 1, 0, 'd' },
      { "createdby", 1, 0, 'b' },
      { "nodate", 0, 0, 'D' },
      { "nocreatedby", 0, 0, 'B' },
      { "md5sum", 0, 0, 'm' },
      { "magnet", 0, 0, 'M' },
      { "quiet", 0, 0, 'q' },
      { "verbose", 0, 0, 'v' },
      { "version", 0, 0, 'V' },
      { "help", 0, 0, 'h' },
      { 0, 0, 0, 0 }
    };
    char c = getopt_long(argc, argv, "hVqvmMBDs:a:f:n:A:N:w:l:L:c:p:d:b:", options, &optidx );
    if (c == (char)-1) {
      break;
    }
    switch (c) {
    case ('?'):
      return 1;
    case ('a'):
      url = optarg;
      break;
    case ('f'):
      filelistfilename = optarg;
      break;
    case ('n'):
      nameflag = optarg;
      break;
    case ('A'):
      urls = optarg;
      break;
    case ('N'):
      nodesstr = optarg;
      break;
    case ('w'):
      wurls = optarg;
      break;
    case ('l'):
      plen = atoi(optarg);
      break;
    case ('L'):
      lplen = atoi(optarg);
      break;
    case ('c'):
      commentstr = optarg;
      break;
    case ('p'):
      privated = 1;
      privateopt = (strcmp(optarg, "0") == 0) ? 0 : 1;
      break;
    case ('s'):
      sort = tolower(optarg[0]);
      switch(sort) {
      default:
#ifdef _GNU_SOURCE
      case 'v':
        /* read directory entry in "version" order, ie. 1 2 10, not 1 10 2 */
        compar = versionsort;
        break;
#endif
      case 'a':
      case 's':
        compar = alphasort;
        break;
      case 'u':
        /* read directory entry normal order (directory order, seemingly random) */
        compar = 0;
        break;
      }
      break;
    case ('d'):
      date = atoll(optarg);
      break;
    case ('D'):
      nodate = 1;
      break;
    case ('b'):
      creator = optarg;
      break;
    case ('B'):
      nocreator = 1;
      break;
    case ('m'):
      domd5sum = 1;
      break;
    case ('M'):
      magnet = 1;
      break;
    case ('v'):
      show++;
      if(show > 2)
        show=2;
      break;
    case ('q'):
      verbose = 0;
      break;
    case ('V'):
      printf(
        "buildtorrent " bt_version "\n"
        "Copyright (C) 2007-2024 Claude Heiland-Allen <claude@mathr.co.uk>\n"
        "License GPLv2+: GNU GPL version 2 or later <http://gnu.org/licenses/gpl.html>\n"
      );
      return 0;
    case ('h'):
      bt_usage();
      return 0;
    }
  }
  if (0 <= lplen && lplen < 31) {
    plen = 1 << lplen;
  }
  if (plen <= 0) { /* avoid division by zero */
    fprintf(stderr, "buildtorrent: piece length must be greater than 0\n");
    return 1;
  }

  if (filelistfilename) {
    if (optind + 1 < argc) {
      fprintf(stderr, "buildtorrent: too many arguments\n");
      fprintf(stderr, "Try `buildtorrent --help' for more information.\n");
      return 1;
    }
    if (optind + 1 > argc) {
      fprintf(stderr, "buildtorrent: too few arguments\n");
      fprintf(stderr, "Try `buildtorrent --help' for more information.\n");
      return 1;
    }
    if (!nameflag) {
      fprintf(stderr, "buildtorrent: missing '-n', required when using '-f'\n");
      fprintf(stderr, "Try `buildtorrent --help' for more information.\n");
      return 1;
    }
    inname = NULL;
    outfile = argv[optind];
  } else {
    if (optind + 2 < argc) {
      fprintf(stderr, "buildtorrent: too many arguments\n");
      fprintf(stderr, "Try `buildtorrent --help' for more information.\n");
      return 1;
    }
    if (optind + 2 > argc) {
      fprintf(stderr, "buildtorrent: too few arguments\n");
      fprintf(stderr, "Try `buildtorrent --help' for more information.\n");
      return 1;
    }
    inname  = argv[optind];
    outfile = argv[optind + 1];
  }

  /* handle paths correctly (note: requires POSIX basename(), not GNU) */
  if (inname) {
    if (strlen(inname) > 8190) {
      fprintf(stderr, "buildtorrent: 'input' argument too long\n");
      return 1;
    }
    strncpy(nametemp, inname, 8191);
    nametemp[8191] = '\0';
    namebase = basename(nametemp);
    slen = strlen(namebase);
    for (i = 0; i < slen; ++i) {
      if (namebase[i] == '/') {
        fprintf(
          stderr,
          "buildtorrent: BUG! input (\"%s\") munged (\"%s\") contains '/'.\n",
          inname,
          namebase
        );
        return 1;
      }
    }
  }

  if (inname) {
    if (stat(inname, &s)) {
      fprintf(stderr, "buildtorrent: could not stat \"%s\"\n", inname);
      return 1;
    }
  }

  torrent = bt_dictionary();
  info = bt_dictionary();
  bt_dictionary_insert0(
    info, "name", bt_string0(nameflag ? nameflag : namebase)
  );
  bt_dictionary_insert0(info, "piece length", bt_integer(plen));
  if (urls) {
    if (!(announcelist = bt_parse_announcelist(urls))) {
      fprintf(stderr, "buildtorrent: error parsing announce-list argument\n");
      return 1;
    }
  }
  if (nodesstr) {
    if (!(nodes = bt_parse_nodes(nodesstr))) {
      fprintf(stderr, "buildtorrent: error parsing nodes argument\n");
      return 1;
    }
  }
  if (wurls) {
    if (!(webseedlist = bt_parse_webseedlist(wurls))) {
      fprintf(stderr, "buildtorrent: error parsing webseed list argument\n");
      return 1;
    }
  }
  flist = bt_create_file_list();
  aflist = bt_create_afile_list();

  if (inname && S_ISDIR(s.st_mode)) {

    multifile = 1;
    pathlist = bt_list();
    memcpy(path, inname, strlen(inname) + 1);
    if (bt_find_files(compar, 8192, path, pathlist, flist)) {
      fprintf(stderr, "buildtorrent: error finding files\n");
      return 1;
    }

  } else if (inname && S_ISREG(s.st_mode)) {

    multifile = 0;
    bt_file_list_prepend(flist, inname, NULL);

  } else if (!inname) {
 
    bt_error err = BT_ERROR_NONE;
    multifile = 1;
    if (0 == strcmp("-", filelistfilename)) {
      err = bt_parse_filelist(flist, stdin);
    } else {
      FILE *filelistfile;
      if ((filelistfile = fopen(filelistfilename, "rb"))) {
        err = bt_parse_filelist(flist, filelistfile);
        fclose(filelistfile);
      } else {
        fprintf(
          stderr,
          "buildtorrent: couldn't open file list \"%s\"\n",
          filelistfilename
        );
        return 1;        
      }
    }
    if (err) {
      fprintf(stderr, "buildtorrent: error processing file list\n");
      return 1;
    }

  } else {

    fprintf(
      stderr, "buildtorrent: \"%s\" is neither file nor directory\n", inname
    );
    return 1;

  }

  if (bt_annotate_files(flist, aflist)) {
    fprintf(stderr, "buildtorrent: error annotating file list\n");
    return 1;
  }

  if (privated) {
    bt_dictionary_insert0(info, "private", bt_integer(privateopt));
  }
  if (url) {
    bt_dictionary_insert0(torrent, "announce", bt_string0(url));
  }
  if (urls) {
    bt_dictionary_insert0(torrent, "announce-list", announcelist);
  }
  if (nodes) {
    bt_dictionary_insert0(torrent, "nodes", nodes);
  }
  if (wurls) {
    bt_dictionary_insert0(torrent, "url-list", webseedlist);
  }
  if (!nodate) {
    bt_dictionary_insert0(torrent, "creation date", bt_integer(date));
  }
  if (!nocreator) {
    bt_dictionary_insert0(
      torrent, "created by", bt_string0(creator)
    );
  }
  if (commentstr) {
    bt_dictionary_insert0(torrent, "comment", bt_string0(commentstr));
  }
  if (!(output = fopen(outfile, "w+b"))) {
    fprintf(
      stderr, "buildtorrent: couldn't open \"%s\" for writing\n", outfile
    );
    return 1;
  }

  if (!(pieces = bt_hash_pieces(aflist, plen, domd5sum, verbose))) {
    fprintf(stderr, "buildtorrent: error hashing files\n");
    return 1;
  }

  if (multifile) {
    bt_dictionary_insert0(info, "files", bt_afile_list_info(aflist));
  } else {
    bt_afile_list node = aflist->next;
    bt_dictionary_insert0(info, "length", bt_integer(node->length));
    if (node->md5sum) {
      bt_dictionary_insert0(info, "md5sum", node->md5sum);
    }
  }

  bt_dictionary_insert0(info, "pieces", pieces);
  bt_dictionary_insert0(torrent, "info", info);

  if (bt_write(output, torrent, 1)) {
    fprintf(stderr, "buildtorrent: error writing \"%s\"\n", outfile);
    return 1;
  }
  if (show) {
    printf("torrent =>\n");
    bt_show(torrent, show == 2, 2, 2, 0);
  }
  if (magnet) {
    if (_bt_write_info_start == -1 || _bt_write_info_end == -1)
    {
      fprintf(stderr, "buildtorrent: error finding info %ld %ld\n", _bt_write_info_start, _bt_write_info_end);
      return 1;
    }
    {
    int i;
    sha1_byte digest[SHA1_DIGEST_LENGTH];
    size_t bytes = _bt_write_info_end - _bt_write_info_start;
    sha1_byte *data = bt_malloc(bytes);
    char *name = nameflag ? nameflag : namebase;
    int n = strlen(name);
    if (0 != fseek(output, _bt_write_info_start, SEEK_SET))
    {
      fprintf(stderr, "buildtorrent: error seeking to info\n");
      return 1;
    }
    if (1 != fread(data, bytes, 1, output)) {
      fprintf(stderr, "buildtorrent: error reading info\n");
      return 1;
    }
    SHA1(data, bytes, digest);
    printf("magnet:?xt=urn:btih:");
    for (i = 0; i < SHA1_DIGEST_LENGTH; ++i) {
      printf("%02X", digest[i]);
    }
    printf("&dn=");
    for (i = 0; i < n; ++i) {
      if (('A' <= name[i] && name[i] <= 'Z') ||
          ('a' <= name[i] && name[i] <= 'z') ||
          ('0' <= name[i] && name[i] <= '9') ||
          '-' == name[i] || '.' == name[i] ||
          '_' == name[i] || '~' == name[i]) {
        putchar(name[i]);
      } else {
        printf("%%%02X", name[i]);
      }
    }
    putchar('\n');
    }
  }
  fclose(output);
  return 0;
}

/* EOF */
